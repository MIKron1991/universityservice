﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UniversityService.Models;

namespace UniversityService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentItemsController : ControllerBase
    {
        public class StudentFilter
        {
            /// <summary>
            /// 0 for Male, 1 - For Female
            /// </summary>
            public StudentItem.Genders? Gender { get; set; }

            public string Name { get; set; }

            public string Nickname { get; set; }

            public string GroupName { get; set; }

            public int PageNumber { get; set; }

            public int PageSize { get; set; }
        }

        private readonly UniversityContext _context;

        public StudentItemsController(UniversityContext context)
        {
            _context = context;
        }

        // GET: api/StudentItems
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<StudentResponse>> GetStudents([FromQuery] StudentFilter f)
        {
            f ??= new StudentFilter();

            var filteredData = (f.Gender == null && string.IsNullOrEmpty(f.Nickname)
                ? _context.Students
                : _context.Students.Where(item =>
                    (f.Gender == null || item.StudentGender == f.Gender) &&
                    (string.IsNullOrEmpty(f.Nickname) || item.StudentNickname == f.Nickname))).Select(item =>
                new StudentItemExt
                {
                    StudentId = item.StudentId,
                    StudentName = string.IsNullOrEmpty(item.StudentNameMiddle)
                        ? $"{item.StudentNameLast} {item.StudentNameFirst}"
                        : $"{item.StudentNameLast} {item.StudentNameFirst} {item.StudentNameMiddle}",
                    StudentNickname = item.StudentNickname,
                    StudentGroups = _context.StudentGroups
                        .Where(_item => _item.StudentId == item.StudentId)
                        .Select(_item => _item.Group.GroupItemName)
                });

            filteredData = string.IsNullOrEmpty(f.Name) && string.IsNullOrEmpty(f.GroupName)
                ? filteredData
                : filteredData.Where(item =>
                    (string.IsNullOrEmpty(f.Name) || item.StudentName == f.Name) &&
                    (string.IsNullOrEmpty(f.GroupName) || item.StudentGroups.Contains(f.GroupName)));

            f.PageSize = f.PageSize > 10 ? 10 : f.PageSize;
            f.PageNumber = f.PageNumber < 1 ? 1 : f.PageNumber;

            var resultData =
                await (f.PageSize == 0
                    ? filteredData
                    : filteredData.Skip((f.PageNumber - 1) * f.PageSize).Take(f.PageSize)).ToListAsync();

            return new StudentResponse
            {
                PageNumber = f.PageNumber,
                PageSize = f.PageSize,
                Data =resultData
            };
        }

        // GET: api/StudentItems/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<StudentItemDto>> GetStudentItem(int id)
        {
            var studentItem = await _context.Students.FindAsync(id);

            if (studentItem == null)
            {
                return NotFound();
            }

            return StudentItemToDto(studentItem);
        }

        // PUT: api/StudentItems/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutStudentItem(int id, StudentItemDto studentItemDto)
        {
            if (id != studentItemDto.StudentId)
            {
                return BadRequest();
            }

            var studentItem = await _context.Students.FindAsync(id);

            if (studentItem == null)
            {
                return NotFound();
            }

            studentItem.StudentGender = studentItemDto.StudentGender;
            studentItem.StudentNameFirst = studentItemDto.StudentNameFirst;
            studentItem.StudentNameMiddle = studentItemDto.StudentNameMiddle;
            studentItem.StudentNameLast = studentItemDto.StudentNameLast;
            studentItem.StudentNickname = studentItemDto.StudentNickname;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception exp)
            {
                return BadRequest(new { errorText = (exp.InnerException ?? exp).Message});
            }

            return NoContent();
        }

        // POST: api/StudentItems
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<StudentItemDto>> PostStudentItem(StudentItemDto studentItemDto)
        {
            var studentItem = new StudentItem
            {
                StudentId = studentItemDto.StudentId,
                StudentGender = studentItemDto.StudentGender,
                StudentNameFirst = studentItemDto.StudentNameFirst,
                StudentNameMiddle = studentItemDto.StudentNameMiddle,
                StudentNameLast = studentItemDto.StudentNameLast,
                StudentNickname = studentItemDto.StudentNickname,
            };

            await _context.Students.AddAsync(studentItem);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception exp)
            {
                if (StudentItemExists(studentItemDto.StudentId))
                {
                    return Conflict(new { errorText = "There is already Student with such studentId." });
                }

                if (StudentNicknameExists(studentItemDto.StudentNickname))
                {
                    return Conflict(new { errorText = "There is already Student with such studentNickname." });
                }

                return BadRequest(new { errorText = (exp.InnerException ?? exp).Message });
            }

            return CreatedAtAction(nameof(GetStudentItem), new { id = studentItem.StudentId }, StudentItemToDto(studentItem));
        }

        // DELETE: api/StudentItems/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult<StudentItemDto>> DeleteStudentItem(int id)
        {
            var studentItem = await _context.Students.FindAsync(id);
            if (studentItem == null)
            {
                return NotFound();
            }

            _context.Students.Remove(studentItem);
            await _context.SaveChangesAsync();

            return StudentItemToDto(studentItem);
        }

        private bool StudentItemExists(int id)
        {
            return _context.Students.Any(e => e.StudentId == id);
        }

        private bool StudentNicknameExists(string nickname)
        {
            return _context.Students.Any(e => e.StudentNickname == nickname);
        }

        private static StudentItemDto StudentItemToDto(StudentItem student) => new StudentItemDto
        {
            StudentId = student.StudentId,
            StudentGender = student.StudentGender,
            StudentNameFirst = student.StudentNameFirst,
            StudentNameMiddle = student.StudentNameMiddle,
            StudentNameLast = student.StudentNameLast,
            StudentNickname = student.StudentNickname,
        };
    }
}
