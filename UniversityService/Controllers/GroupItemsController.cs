﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UniversityService.Models;

namespace UniversityService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupItemsController : ControllerBase
    {
        private readonly UniversityContext _context;

        public GroupItemsController(UniversityContext context)
        {
            _context = context;
        }

        // GET: api/GroupItems
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<GroupItemExt>>> GetGroups(string groupName)
        {
            return await (string.IsNullOrEmpty(groupName)
                    ? _context.Groups
                    : _context.Groups
                        .Where(item => item.GroupItemName == groupName))
                .Select(item => new GroupItemExt
                {
                    GroupItemId = item.GroupItemId,
                    GroupItemName = item.GroupItemName,
                    GroupStudentCount = _context.StudentGroups.Count(_item => _item.GroupId == item.GroupItemId),
                })
                .ToListAsync();
        }

        // GET: api/GroupItems/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<GroupItemDto>> GetGroupItem(int id)
        {
            var groupItem = await _context.Groups.FindAsync(id);

            if (groupItem == null)
            {
                return NotFound();
            }

            return GroupItemToDto(groupItem);
        }

        // PUT: api/GroupItems/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutGroupItem(int id, GroupItemDto groupItemDto)
        {
            if (id != groupItemDto.GroupItemId)
            {
                return BadRequest();
            }

            var groupItem = await _context.Groups.FindAsync(id);
            if (groupItem == null)
            {
                return NotFound();
            }

            groupItem.GroupItemName = groupItemDto.GroupItemName;
            
            //_context.Entry(groupItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception exp)
            {
                if (!GroupItemExists(id))
                {
                    return NotFound();
                }

                return BadRequest(new { errorText = (exp.InnerException ?? exp).Message });
            }

            return NoContent();
        }

        // POST: api/GroupItems
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<GroupItemDto>> PostGroupItem(GroupItemDto groupItemDto)
        {
            var groupItem = new GroupItem
            {
                GroupItemId = groupItemDto.GroupItemId,
                GroupItemName = groupItemDto.GroupItemName
            };

            await _context.Groups.AddAsync(groupItem);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception exp)
            {
                if (GroupItemExists(groupItemDto.GroupItemId))
                {
                    return Conflict();
                }

                return BadRequest(new {errorText = (exp.InnerException??exp).Message});
            }

            return CreatedAtAction(nameof(GetGroupItem), new { id = groupItem.GroupItemId }, GroupItemToDto(groupItem));
        }

        // DELETE: api/GroupItems/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult<GroupItemDto>> DeleteGroupItem(int id)
        {
            var groupItem = await _context.Groups.FindAsync(id);
            if (groupItem == null)
            {
                return NotFound();
            }

            _context.Groups.Remove(groupItem);
            await _context.SaveChangesAsync();

            return GroupItemToDto(groupItem);
        }

        private bool GroupItemExists(int id)
        {
            return _context.Groups.Any(e => e.GroupItemId == id);
        }

        private static GroupItemDto GroupItemToDto(GroupItem group) => new GroupItemDto
        {
            GroupItemId = group.GroupItemId,
            GroupItemName = group.GroupItemName
        };
    }
}