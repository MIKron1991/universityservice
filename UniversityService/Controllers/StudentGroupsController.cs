﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UniversityService.Models;

namespace UniversityService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentGroupsController : ControllerBase
    {
        private readonly UniversityContext _context;

        public StudentGroupsController(UniversityContext context)
        {
            _context = context;
        }

        // POST: api/StudentGroups
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<StudentGroupDto>> PostStudentGroup(StudentGroupDto studentGroup)
        {
            var studentItem = await _context.Students.FindAsync(studentGroup.StudentId);
            
            if (studentItem == null)
            {
                return NotFound("studentId");
            }

            var groupItem = await _context.Groups.FindAsync(studentGroup.GroupId);

            if (groupItem == null)
            {
                return NotFound("groupId");
            }

            var resultStudentGroup = new StudentGroup
            {
                Student = studentItem,
                Group = groupItem
            };

            studentItem.StudentGroups ??= new List<StudentGroup>();
            studentItem.StudentGroups.Add(resultStudentGroup);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch
            {
                if (StudentGroupExists(studentGroup))
                {
                    return Conflict();
                }

                throw;
            }

            return studentGroup;
        }

        //DELETE: api/StudentGroups/1/1
        [HttpDelete("{sid}/{gid}")]
        [Authorize]
        public async Task<ActionResult<StudentGroupDto>> DeleteStudentGroup(int sid, int gid)
        {
            var studentGroup = await _context.StudentGroups.FindAsync(sid, gid);
            if (studentGroup == null)
            {
                return NotFound();
            }

            _context.StudentGroups.Remove(studentGroup);

            await _context.SaveChangesAsync();

            return new StudentGroupDto
            {
                GroupId = studentGroup.GroupId,
                StudentId = studentGroup.StudentId
            };
        }

        private bool StudentGroupExists(StudentGroupDto studentGroup)
        {
            return _context.StudentGroups.Any(e => e.StudentId == studentGroup.StudentId && e.GroupId == studentGroup.GroupId);
        }
    }
}