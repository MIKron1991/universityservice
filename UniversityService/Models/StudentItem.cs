﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UniversityService.Models
{
    public class StudentItem
    {
        public enum Genders
        {
            Male,
            Female
        }

        [Key]
        public int StudentId { get; set; }
        
        [Required]
        public Genders StudentGender { get; set; }
        
        [Required]
        [MaxLength(40)]
        public string StudentNameLast { get; set; }
        
        [Required]
        [MaxLength(40)]
        public string StudentNameFirst { get; set; }

        [MaxLength(60)]
        public string StudentNameMiddle { get; set; }

        [MinLength(6)]
        [MaxLength(16)]
        public string StudentNickname { get; set; }

        public virtual ICollection<StudentGroup> StudentGroups { get; set; }
    }

    public class StudentItemDto
    {
        public int StudentId { get; set; }

        [Required]
        public StudentItem.Genders StudentGender { get; set; }

        [Required]
        [MaxLength(40)]
        public string StudentNameLast { get; set; }

        [Required]
        [MaxLength(40)]
        public string StudentNameFirst { get; set; }

        [MaxLength(60)]
        public string StudentNameMiddle { get; set; }

        [MinLength(6)]
        [MaxLength(16)]
        public string StudentNickname { get; set; }
    }

    public class StudentItemExt
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string StudentNickname { get; set; }
        public IEnumerable <string> StudentGroups { get; set; }
    }

    public class StudentResponse
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public IEnumerable<StudentItemExt> Data { get; set; }
    }
}
