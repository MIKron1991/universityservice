﻿using Microsoft.EntityFrameworkCore;

namespace UniversityService.Models
{
    public class UniversityContext : DbContext
    {
        public UniversityContext(DbContextOptions<UniversityContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured)
            {
                options.UseSqlite("Data Source=University.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StudentItem>()
                .HasIndex(b => b.StudentNickname)
                .IsUnique()
                .HasFilter(null);

            modelBuilder.Entity<StudentGroup>().HasKey(sc => new { sc.StudentId, sc.GroupId });

            modelBuilder.Entity<StudentGroup>()
                .HasOne(sc => sc.Student)
                .WithMany(s => s.StudentGroups)
                .HasForeignKey(sg => sg.StudentId);


            modelBuilder.Entity<StudentGroup>()
                .HasOne(sc => sc.Group)
                .WithMany(s => s.StudentGroups)
                .HasForeignKey(sg => sg.GroupId);
        }

        public DbSet<StudentItem> Students { get; set; }
        public DbSet<GroupItem> Groups { get; set; }
        public DbSet<StudentGroup> StudentGroups { get; set; }
    }
}
