﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace UniversityService.Models
{
    public class Person
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }

    public class AuthOptions
    {
        public const string ISSUER = "MyAuthServer";
        
        public const string AUDIENCE = "MyAuthClient";

        private const string KEY = "mysupersecret_secretkey!123";
        
        /// <summary>
        /// Token lifetime in minutes
        /// </summary>
        public const int LIFETIME = 10;
        
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
