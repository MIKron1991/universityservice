﻿namespace UniversityService.Models
{
    public class StudentGroup
    {
        public int StudentId { get; set; }
        public StudentItem Student { get; set; }

        public int GroupId { get; set; }
        public GroupItem Group { get; set; }
    }

    public class StudentGroupDto
    {
        public int StudentId { get; set; }
        public int GroupId { get; set; }
    }
}