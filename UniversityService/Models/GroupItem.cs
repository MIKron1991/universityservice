﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UniversityService.Models
{
    public class GroupItem
    {
        [Key]
        public int GroupItemId { get; set; }

        [Required]
        [MaxLength(25)]
        public string GroupItemName { get; set; }

        public virtual ICollection<StudentGroup> StudentGroups { get; set; }
    }

    public class GroupItemDto
    {
        public int GroupItemId { get; set; }
        
        [Required]
        [MaxLength(25)]
        public string GroupItemName { get; set; }
    }

    public class GroupItemExt
    {
        public int GroupItemId { get; set; }
        public string GroupItemName { get; set; }
        public int GroupStudentCount { get; set; }
    }
}
