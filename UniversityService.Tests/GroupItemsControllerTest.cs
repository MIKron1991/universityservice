using System;
using System.Data.Common;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using UniversityService.Controllers;
using UniversityService.Models;
using Xunit;

namespace UniversityService.Tests
{
    public class GroupItemsControllerTest : TestBase, IDisposable
    {
        private readonly DbConnection _connection;

        public GroupItemsControllerTest() : base(
            new DbContextOptionsBuilder<UniversityContext>()
                .UseSqlite(CreateInMemoryDatabase())
                .Options)
        {
            _connection = RelationalOptionsExtension.Extract(ContextOptions).Connection;

            context = new UniversityContext(ContextOptions);

            controller = new GroupItemsController(context);
        }

        public void Dispose() => _connection.Dispose();

        public UniversityContext context { get; set; }

        public GroupItemsController controller { get; set; }

        [Fact]
        public void Get_Items()
        {
            var items = controller.GetGroups(string.Empty).Result.Value.ToList();

            Assert.Equal(2, items.Count);
            Assert.Equal("Group 1", items[0].GroupItemName);
            Assert.Equal("Group 2", items[1].GroupItemName);
        }

        [Fact]
        public void Get_Items_With_Filter()
        {
            var items = controller.GetGroups("Group 2").Result.Value.ToList();

            Assert.Single(items);
            Assert.Equal("Group 2", items[0].GroupItemName);
        }

        [Fact]
        public void Get_Item()
        {
            var item = controller.GetGroupItem(2).Result.Value;

            Assert.NotNull(item);
            Assert.Equal("Group 2", item.GroupItemName);
        }

        [Fact]
        public void Get_Item_Not_Exists()
        {
            var result = controller.GetGroupItem(3).Result.Result;

            Assert.IsAssignableFrom<NotFoundResult>(result);
        }

        [Fact]
        public void Post_Item()
        {
            var item = (controller.PostGroupItem(new GroupItemDto
            {
                GroupItemName = "Group 3"
            }).Result.Result as CreatedAtActionResult)?.Value;

            Assert.NotNull(item);
            Assert.Equal("Group 3", (item as GroupItemDto)?.GroupItemName);

            var items = controller.GetGroups(string.Empty).Result.Value.ToList();

            Assert.Equal(3, items.Count);
            Assert.Equal("Group 3", items[2].GroupItemName);
        }

        [Fact]
        public void Post_Item_Same_Id()
        {
            var result = controller.PostGroupItem(new GroupItemDto
            {
                GroupItemId = 2,
                GroupItemName = "Group 4"
            }).Result.Result;

            Assert.IsAssignableFrom<ConflictResult>(result);
        }

        [Fact]
        public async void Put_Item()
        {
            await controller.PutGroupItem(2, new GroupItemDto
            {
                GroupItemId = 2,
                GroupItemName = "Group 2 (modified)"
            });

            var item = controller.GetGroupItem(2).Result.Value;

            Assert.Equal("Group 2 (modified)", item.GroupItemName);
        }

        [Fact]
        public void Put_Item_Bad_Request()
        {
            var result = controller.PutGroupItem(3, new GroupItemDto
            {
                GroupItemId = 2,
                GroupItemName = "Group 2 (modified)"
            }).Result;

            Assert.IsAssignableFrom<BadRequestResult>(result);
        }

        [Fact]
        public void Put_Item_Not_Exists()
        {
            var result = controller.PutGroupItem(3, new GroupItemDto
            {
                GroupItemId = 3,
                GroupItemName = "Group 2 (modified)"
            }).Result;

            Assert.IsAssignableFrom<NotFoundResult>(result);
        }

        [Fact]
        public void Delete_Item()
        {
            var item = controller.DeleteGroupItem(1).Result.Value;

            Assert.NotNull(item);
            Assert.Equal("Group 1", item.GroupItemName);
            Assert.Equal(1, item.GroupItemId);
        }

        [Fact]
        public void Delete_Item_Not_Exists()
        {
            var result = controller.DeleteGroupItem(3).Result.Result;

            Assert.IsAssignableFrom<NotFoundResult>(result);
        }
    }
}
