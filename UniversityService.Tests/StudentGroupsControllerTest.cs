using System;
using System.Data.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using UniversityService.Controllers;
using UniversityService.Models;
using Xunit;

namespace UniversityService.Tests
{
    public class StudentGroupsControllerTest : TestBase, IDisposable
    {
        private readonly DbConnection _connection;

        public StudentGroupsControllerTest() : base(
            new DbContextOptionsBuilder<UniversityContext>()
                .UseSqlite(CreateInMemoryDatabase())
                .Options)
        {
            _connection = RelationalOptionsExtension.Extract(ContextOptions).Connection;

            context = new UniversityContext(ContextOptions);

            controller = new StudentGroupsController(context);
        }

        public void Dispose() => _connection.Dispose();

        public UniversityContext context { get; set; }

        public StudentGroupsController controller { get; set; }

        [Fact]
        public void Enroll_Student()
        {
            var item = controller.PostStudentGroup(new StudentGroupDto
            {
                GroupId = 1,
                StudentId = 1
            }).Result.Value;

            Assert.NotNull(item);
            Assert.Equal(1, item.StudentId);
            Assert.Equal(1, item.GroupId);
        }

        [Fact]
        public void Enroll_Student_Not_Found()
        {
            var result = controller.PostStudentGroup(new StudentGroupDto
            {
                GroupId = 3,
                StudentId = 4
            }).Result.Result;

            Assert.IsAssignableFrom<NotFoundObjectResult>(result);
        }

        [Fact]
        public void Enroll_Student_Same_Id()
        {
            var result = controller.PostStudentGroup(new StudentGroupDto
            {
                GroupId = 1,
                StudentId = 3
            }).Result.Result;

            Assert.IsAssignableFrom<ConflictResult>(result);
        }

        [Fact]
        public void Expel_Student()
        {
            var item = controller.DeleteStudentGroup(3, 1).Result.Value;

            Assert.NotNull(item);
            Assert.Equal(3, item.StudentId);
            Assert.Equal(1, item.GroupId);
        }

        [Fact]
        public void Expel_Student_Not_Found()
        {
            var result = controller.DeleteStudentGroup(1, 1).Result.Result;

            Assert.IsAssignableFrom<NotFoundResult>(result);
        }
    }
}
