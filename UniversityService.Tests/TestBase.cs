﻿using System.Collections.Generic;
using System.Data.Common;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using UniversityService.Models;

namespace UniversityService.Tests
{
    public class TestBase
    {
        private static readonly object _lock = new object();

        protected TestBase(DbContextOptions<UniversityContext> contextOptions)
        {
            ContextOptions = contextOptions;

            Seed();
        }

        protected static DbConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");

            connection.Open();

            return connection;
        }

        protected DbContextOptions<UniversityContext> ContextOptions { get; }

        private void Seed()
        {
            lock (_lock)
            {
                using var context = new UniversityContext(ContextOptions);

                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                object[] Students =
                {
                    new StudentItem
                    {
                        StudentGender = StudentItem.Genders.Male,
                        StudentNameFirst = "Jack",
                        StudentNameLast = "Jonson",
                        StudentNickname = "Nick1"
                    },
                    new StudentItem
                    {
                        StudentGender = StudentItem.Genders.Male,
                        StudentNameFirst = "Egor",
                        StudentNameMiddle = "Ivanovich",
                        StudentNameLast = "Petrov",
                        StudentNickname = "Nick2"
                    },
                    new StudentItem
                    {
                        StudentGender = StudentItem.Genders.Female,
                        StudentNameFirst = "Anna",
                        StudentNameLast = "Jackman",
                        StudentNickname = "Nick3"
                    },
                };

                object[] Groups =
                {
                    new GroupItem
                    {
                        GroupItemName = "Group 1",
                    },
                    new GroupItem
                    {
                        GroupItemName = "Group 2",
                    },
                };

                var group = (GroupItem) Groups[0];

                var student = (StudentItem) Students[2];

                group.StudentGroups = new List<StudentGroup>
                {
                    new StudentGroup
                    {
                        Group = group,
                        Student = student
                    }
                };

                context.AddRange(Students);
                context.AddRange(Groups);
                context.SaveChanges();
            }
        }
    }
}
