using System;
using System.Data.Common;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using UniversityService.Controllers;
using UniversityService.Models;
using Xunit;

namespace UniversityService.Tests
{
    public class StudentItemsControllerTest : TestBase, IDisposable
    {
        private readonly DbConnection _connection;

        public StudentItemsControllerTest() : base(
            new DbContextOptionsBuilder<UniversityContext>()
                .UseSqlite(CreateInMemoryDatabase())
                .Options)
        {
            _connection = RelationalOptionsExtension.Extract(ContextOptions).Connection;

            context = new UniversityContext(ContextOptions);

            controller = new StudentItemsController(context);
        }

        public void Dispose() => _connection.Dispose();

        public UniversityContext context { get; set; }

        public StudentItemsController controller { get; set; }

        [Fact]
        public void Get_Items()
        {
            var response = controller.GetStudents(null).Result.Value;

            var items = response.Data.ToList();

            Assert.Equal(3, items.Count);
            Assert.Equal("Nick1", items[0].StudentNickname);
            Assert.Equal("Nick2", items[1].StudentNickname);
            Assert.Equal("Nick3", items[2].StudentNickname);
        }

        [Fact]
        public void Get_Items_With_Filter()
        {
            var response = controller.GetStudents(new StudentItemsController.StudentFilter
            {
                Gender = StudentItem.Genders.Male,
                PageNumber = 2,
                PageSize = 1,
            }).Result.Value;

            var items = response.Data.ToList();

            Assert.Single(items);
            Assert.Equal("Petrov Egor Ivanovich", items[0].StudentName);
        }

        [Fact]
        public void Get_Item()
        {
            var item = controller.GetStudentItem(2).Result.Value;

            Assert.NotNull(item);
            Assert.Equal("Nick2", item.StudentNickname);
        }

        [Fact]
        public void Get_Item_Not_Exists()
        {
            var result = controller.GetStudentItem(4).Result.Result;

            Assert.IsAssignableFrom<NotFoundResult>(result);
        }

        [Fact]
        public void Post_Item()
        {
            var item = (controller.PostStudentItem(new StudentItemDto
            {
                StudentGender = StudentItem.Genders.Female,
                StudentNameLast = "Romanova",
                StudentNameMiddle = "Petrovna",
                StudentNameFirst = "Olga",
                StudentNickname = "Nick4"
            }).Result.Result as CreatedAtActionResult)?.Value;

            Assert.NotNull(item);
            Assert.Equal("Romanova", (item as StudentItemDto)?.StudentNameLast);

            var items = controller.GetStudents(null).Result.Value.Data.ToList();

            Assert.Equal(4, items.Count);
            Assert.Equal("Nick4", items[3].StudentNickname);
        }

        [Fact]
        public void Post_Item_Same_Id()
        {
            var result = controller.PostStudentItem(new StudentItemDto
            {
                StudentId = 2,
                StudentGender = StudentItem.Genders.Female,
                StudentNameLast = "Romanova",
                StudentNameMiddle = "Petrovna",
                StudentNameFirst = "Olga",
                StudentNickname = "Nick4"
            }).Result.Result;

            Assert.IsAssignableFrom<ConflictObjectResult>(result);
        }

        [Fact]
        public void Post_Item_Same_Nickname()
        {
            var result = controller.PostStudentItem(new StudentItemDto
            {
                StudentGender = StudentItem.Genders.Female,
                StudentNameLast = "Romanova",
                StudentNameMiddle = "Petrovna",
                StudentNameFirst = "Olga",
                StudentNickname = "Nick3"
            }).Result.Result;

            Assert.IsAssignableFrom<ConflictObjectResult>(result);
        }

        [Fact]
        public void Post_Item_Null_Last_Name()
        {
            var result = controller.PostStudentItem(new StudentItemDto
            {
                StudentGender = StudentItem.Genders.Female,
                StudentNameLast = null,
                StudentNameMiddle = "Petrovna",
                StudentNameFirst = "Olga",
            }).Result.Result;

            Assert.IsAssignableFrom<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void Put_Item()
        {
            await controller.PutStudentItem(1, new StudentItemDto
            {
                StudentId = 1,
                StudentGender = StudentItem.Genders.Male,
                StudentNameFirst = "Jack",
                StudentNameLast = "Jonson-Phillips",
                StudentNickname = "Nick1"
            });

            var item = controller.GetStudentItem(1).Result.Value;

            Assert.Equal("Jonson-Phillips", item.StudentNameLast);
        }

        [Fact]
        public void Put_Item_Bad_Request()
        {
            var result = controller.PutStudentItem(3, new StudentItemDto
            {
                StudentId = 1,
                StudentGender = StudentItem.Genders.Male,
                StudentNameFirst = "Jack",
                StudentNameLast = "Jonson-Phillips",
                StudentNickname = "Nick1"
            }).Result;

            Assert.IsAssignableFrom<BadRequestResult>(result);
        }

        [Fact]
        public void Put_Item_Not_Exists()
        {
            var result = controller.PutStudentItem(4, new StudentItemDto
            {
                StudentId = 4,
                StudentGender = StudentItem.Genders.Male,
                StudentNameFirst = "Jack",
                StudentNameLast = "Jonson-Phillips",
                StudentNickname = "Nick1"
            }).Result;

            Assert.IsAssignableFrom<NotFoundResult>(result);
        }

        [Fact]
        public void Delete_Item()
        {
            var item = controller.DeleteStudentItem(3).Result.Value;

            Assert.NotNull(item);
            Assert.Equal("Jackman", item.StudentNameLast);
            Assert.Equal(3, item.StudentId);
        }

        [Fact]
        public void Delete_Item_Not_Exists()
        {
            var result = controller.DeleteStudentItem(4).Result.Result;

            Assert.IsAssignableFrom<NotFoundResult>(result);
        }
    }
}
